﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFApp
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            
        }

        

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void filmesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool IsOpen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Text == "Filmes")
                {
                    IsOpen = true;
                    f.Focus();
                    break;
                }
            }

            if (IsOpen == false) 
            {
                frm_Filmes childfrm = new frm_Filmes();
                childfrm.MdiParent = this;
                childfrm.WindowState = FormWindowState.Maximized;
                childfrm.Show();
            }

        }

        private void atoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //bool IsOpen = false;
            //foreach (Form f in Application.OpenForms)
            //{
            //    if (f.Text == "Atores")
            //    {
            //        IsOpen = true;
            //        f.Focus();
            //        break;
            //    }
            //}

            //if (IsOpen == false)
            //{
            //    frm_Atores childfrm = new frm_Atores();
            //    childfrm.MdiParent = this;
            //    childfrm.WindowState = FormWindowState.Maximized;
            //    childfrm.Show();
            //}

        }

        private void fecharTudoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form f in this.MdiChildren)
            {
                if (!f.Focused)
                {
                    f.Visible = false;
                    f.Dispose();                    
                }
            }

        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.Close();
            Application.Exit();
        }
    }
}
