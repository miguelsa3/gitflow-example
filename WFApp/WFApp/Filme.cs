﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFApp
{
    public class Filme
    {
        public string title { get; set; }
        public string description { get; set; }
        public string year { get; set; }

        public Filme() { }

        public Filme(string title, string description, string year)
        {
            this.title = title;
            this.description = description;
            this.year = year;
        }
    }
}
