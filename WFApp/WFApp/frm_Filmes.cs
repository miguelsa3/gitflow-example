﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFApp
{
    public partial class frm_Filmes : Form
    {
        private static HttpClient ApiClient { get; set; }

        public frm_Filmes()
        {
            InitializeComponent();
            InitializeClient("https://localhost:44326/API/");
            LoadTable();
        }

        public static void InitializeClient(string url)
        {
            ApiClient = new HttpClient();
            Uri enderecoBase = new Uri(url);
            ApiClient.BaseAddress = enderecoBase;

            ApiClient.DefaultRequestHeaders.Accept.Clear();

            //a linha em baixo, identifica que quer todos os dados em formato JSON
            ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        //Load da Tabela Filmes
        public async void LoadTable()
        {
            string uri = "movies/get";

            using (ApiClient)
            {
                using (var response = await ApiClient.GetAsync(uri))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var JsonString = await response.Content.ReadAsStringAsync();

                        APIResult res = JsonConvert.DeserializeObject<APIResult>(JsonString);
                        List<Filme> list = JsonConvert.DeserializeObject<List<Filme>>(res.Result.ToString());

                        dataGridView1.DataSource = list;

                        //Definições para a tabela
                        dataGridView1.Columns["title"].HeaderText = "Título";
                        dataGridView1.Columns["description"].HeaderText = "Descrição";
                        dataGridView1.Columns["year"].HeaderText = "Ano";
                        dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    }
                    else
                    {
                        MessageBox.Show("Não foi possível obter o produto : " + response.StatusCode);
                    }
                }
            }

        }

        private void Filmes_Load(object sender, EventArgs e)
        {

        }
    }
}
