﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Actor
    {
        public string first_name { get; set; }
        public string last_name { get; set; }

        public Actor() { }

        public Actor(string first_name, string last_name)
        {
            this.first_name = first_name;
            this.last_name = last_name;
        }
    }
}
