﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Movie
    {
        public string title { get; set; }
        public string description { get; set; }
        public string year { get; set; }

        public Movie(){}

        public Movie(string title, string description, string year)
        {
            this.title = title;
            this.description = description;
            this.year = year;
        }

    }
}
