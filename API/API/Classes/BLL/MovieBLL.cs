﻿using API.Classes.DLL;
using API.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace API.Classes.BLL
{
    public class MovieBLL
    {
        public async static Task<List<Movie>> GetMovies()
        {
            SqlConnection connection = null;
            SqlDataReader rs = null;
            List<Movie> movies = new List<Movie>();

            try
            {
                connection = await BDConnection.GetConnection();
                SqlCommand query = new SqlCommand("SELECT title, description, release_year FROM film", connection);

                rs = query.ExecuteReader();

                while (rs.Read())
                {
                    movies.Add(new Movie(rs.GetString(0), rs.GetString(1), rs.GetString(2)));
                }

                rs.Close();
                connection.Close();

            }
            catch (Exception e)
            {

                if (rs != null)
                {
                    rs.Close();
                }

                if (connection != null)
                {
                    connection.Close();
                }

                throw new Exception(e.Message);
            }
            return movies;
        }

    }
}
