﻿using API.Classes.DLL;
using API.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace API.Classes.BLL
{
    public class ActorBLL
    {
        public static async Task<List<Actor>> GetActors()
        {
            SqlConnection connection = null;
            SqlDataReader rs = null;
            List<Actor> actors = new List<Actor>();

            try
            {
                connection = await BDConnection.GetConnection();
                SqlCommand query = new SqlCommand("SELECT first_name, last_name FROM actor", connection);
                rs = query.ExecuteReader();

                while (rs.Read())
                {
                    actors.Add(new Actor(rs.GetString(0), rs.GetString(1)));
                }

                rs.Close();
                connection.Close();

            }
            catch (Exception e)
            {
                if (rs != null)
                {
                    rs.Close();
                }
                if (connection != null)
                {
                    connection.Close();
                }
                throw new Exception(e.Message);
            }

            return actors;
        }

    }
}
