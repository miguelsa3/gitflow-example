﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace API.Classes.DLL
{
    public class BDConnection
    {
        public async static Task<SqlConnection> GetConnection()
        {
            SqlConnection connection = null;
            try
            {
                string connectionString = "Data Source=PNTLAB\\SQL2017;Initial Catalog=movieDB;User ID=sa;Password=Secret25;MultipleActiveResultSets=true;";
                connection = new SqlConnection(connectionString);
                connection.Open();
            }
            catch (Exception)
            {

            }

            return connection;
        }

    }
}
